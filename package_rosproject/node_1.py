## INITITIATION ROS2 ROBOTIQUE
##Itonia Bissielou

import rclpy
from rclpy.node import Node

from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState
from sensor_msgs.msg import JointState, Range, Image


class EPuckController(Node):
    def __init__(self):
        super().__init__('epuck_controller')

        self.cmd_publisher_ = self.create_publisher(
            Float64MultiArray, '/wheels_velocity_controller/commands', 10)

        self.joint_state_subscription_ = self.create_subscription(
            JointState,
            '/joint_states',
            self.joint_state_callback,
            10)
        
        
        self.ps0_subscription_ = self.create_subscription(
            Range,
            '/ps0/out',
            self.ps0_callback,
            10)
        self.ps1_subscription_ = self.create_subscription(
            Range,
            '/ps1/out',
            self.ps1_callback,
            10)
        self.ps2_subscription_ = self.create_subscription(
            Range,
            '/ps2/out',
            self.ps2_callback,
            10)
        self.ps3_subscription_ = self.create_subscription(
            Range,
            '/ps3/out',
            self.ps3_callback,
            10)
        self.ps4_subscription_ = self.create_subscription(
            Range,
            '/ps4/out',
            self.ps4_callback,
            10)
        self.ps5_subscription_ = self.create_subscription(
            Range,
            '/ps5/out',
            self.ps5_callback,
            10)
        self.ps6_subscription_ = self.create_subscription(
            Range,
            '/ps6/out',
            self.ps6_callback,
            10)
        self.ps7_subscription_ = self.create_subscription(
            Range,
            '/ps7/out',
            self.ps7_callback,
            10)
            
        
        self.camera_subscription_ = self.create_subscription(
            Image,
            '/camera/image_raw',
            self.camera_callback,
            10)
        
        self.cmd_ = Float64MultiArray(data=[0, 0])
        
        
        self.ps0 = 1000
        self.ps1 = 1000
        self.ps2 = 1000
        self.ps3 = 1000
        self.ps4 = 1000
        self.ps5 = 1000
        self.ps6 = 1000
        self.ps7 = 1000
    
        
        self.start_time_ = self.get_clock().now().seconds_nanoseconds()[0]
        self.run_timer_ = self.create_timer(0.1, self.run)

    def run(self):
        self.cmd_.data = [-3.0, -3.0]
#        if self.get_clock().now().seconds_nanoseconds()[0] < self.start_time_ + 0.0:
#            self.cmd_.data = [1.0, 1.0]
#        else:
#            self.cmd_.data = [1.0, -3.0]
        
        if (self.ps0 < 1.5 or self.ps1 < 1.5 or self.ps6 < 1.5 or self.ps7 < 1.5) :
             self.cmd_.data = [3.0, 3.0]
        else:
            self.cmd_.data = [-3.0, -3.0]
             
        if self.ps2 < 1.5 or self.ps5 < 1.5:
            self.cmd_.data = [3.0, -3.0]
        else:
            self.cmd_.data = [-3.0, -3.0]
            
        if self.ps3 < 1.5 or self.ps4 < 1.5:
            self.cmd_.data = [-3.0, -3.0]
        else:
            self.cmd_.data = [-3.0, -3.0]
            
            
#        if self.ps0 < 1.5 or self.ps1 < 1.5 or self.ps2 < 1.5 or self.ps3 < 1.5 or self.ps4 < 1.5 or self.ps2 < 1.5 or self.ps6 < 1.5 or self.ps7 < 1.5 :
#            self.cmd_.data = [0.0, 0.0]
#            if self.get_clock().now().seconds_nanoseconds()[0] > self.start_time_ + 0.5:
#                self.cmd_.data = [1.0, -1.0]
#            else:
#                self.cmd_.data = [0.0, 0.0]
#        else:
#            self.cmd_.data = [1.0, 1.0]
        
            

        self.cmd_publisher_.publish(self.cmd_)

    def joint_state_callback(self, msg):
        self.get_logger().info(
            "\nWheels\n\tname: %s\n\tposition: %s\n\tvelocity: %s" %
            (msg.name, msg.position, msg.velocity))
        
    
    def ps0_callback(self, msg):
        print("ps0: %s" % msg.range)
        self.ps0 = msg.range
    def ps1_callback(self, msg):
        print("ps1: %s" % msg.range)
        self.ps1 = msg.range
    def ps2_callback(self, msg):
        print("ps2: %s" % msg.range)
        self.ps2 = msg.range
    def ps3_callback(self, msg):
        print("ps3: %s" % msg.range)
        self.ps3 = msg.range
    def ps4_callback(self, msg):
        print("ps4: %s" % msg.range)
        self.ps4 = msg.range
    def ps5_callback(self, msg):
        print("ps5: %s" % msg.range)
        self.ps5 = msg.range
    def ps6_callback(self, msg):
        print("ps6: %s" % msg.range)
        self.ps6 = msg.range
    def ps7_callback(self, msg):
        print("ps7: %s" % msg.range)
        self.ps7 = msg.range
    
    
    
    def camera_callback(self, msg):
        pass



def main(args=None):
    rclpy.init(args=args)

    epuck_controller = EPuckController()

    try:
        rclpy.spin(epuck_controller)
    finally:
        rclpy.shutdown()


if __name__ == '__main__':
    print('Hi from package_rosproject. I am Itonia Bissielou, in robotic classes with M.Navarro!')
    main()

